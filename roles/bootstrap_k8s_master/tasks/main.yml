---

- name: Clean Working Directory 
  file:
    state: absent
    path: "{{ remote_workspace }}"

- name: Creates Deployment Working Directory
  file:
    path: "{{ remote_workspace }}"
    state: directory

- name: Download and Extract k8s packages (api,controller,scheduler,kubectl)
  shell:
    cmd: |
      cd {{ remote_workspace }} && wget -q --show-progress --https-only --timestamping \
      "https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kube-apiserver" \
      "https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kube-controller-manager" \
      "https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kube-scheduler" \
      "https://storage.googleapis.com/kubernetes-release/release/v1.18.6/bin/linux/amd64/kubectl"
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Move k8s binaries to /usr/local/bin
  shell:
    cmd: |
      chmod +x {{ remote_workspace }}/kube-apiserver {{ remote_workspace }}/kube-controller-manager {{ remote_workspace }}/kube-scheduler {{ remote_workspace }}/kubectl
      cp {{ remote_workspace }}/kube-apiserver {{ remote_workspace }}/kube-controller-manager {{ remote_workspace }}/kube-scheduler {{ remote_workspace }}/kubectl /usr/local/bin/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Update k8s config & Copy Certificates
  shell:
    cmd: |
      mkdir -p /etc/kubernetes/config
      mkdir -p /var/lib/kubernetes/
      cp {{ remote_user_home }}/ca.pem {{ remote_user_home }}/ca-key.pem {{ remote_user_home }}/kubernetes-key.pem {{ remote_user_home }}/kubernetes.pem \
       {{ remote_user_home }}/service-account-key.pem {{ remote_user_home }}/service-account.pem \
       {{ remote_user_home }}/encryption-config.yaml /var/lib/kubernetes/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Add kube-apiserver service to systemd
  shell:
    cmd: |
      INTERNAL_IP={{ ansible_default_ipv4.address }}
      cat <<EOF | tee /etc/systemd/system/kube-apiserver.service
      [Unit]
      Description=Kubernetes API Server
      Documentation=https://github.com/kubernetes/kubernetes

      [Service]
      ExecStart=/usr/local/bin/kube-apiserver \\
        --advertise-address=${INTERNAL_IP} \\
        --allow-privileged=true \\
        --apiserver-count=3 \\
        --audit-log-maxage=30 \\
        --audit-log-maxbackup=3 \\
        --audit-log-maxsize=100 \\
        --audit-log-path=/var/log/audit.log \\
        --authorization-mode=Node,RBAC \\
        --bind-address=0.0.0.0 \\
        --client-ca-file=/var/lib/kubernetes/ca.pem \\
        --enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
        --etcd-cafile=/var/lib/kubernetes/ca.pem \\
        --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
        --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
        --etcd-servers=https://{{ hostvars['k8s-master-1']['ansible_ssh_host'] }}:2379,https://{{ hostvars['k8s-master-2']['ansible_ssh_host'] }}:2379,https://{{ hostvars['k8s-master-3']['ansible_ssh_host'] }}:2379 \\
        --event-ttl=1h \\
        --encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
        --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
        --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
        --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
        --kubelet-https=true \\
        --runtime-config='api/all=true' \\
        --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
        --service-cluster-ip-range=10.32.0.0/24 \\
        --service-node-port-range=30000-32767 \\
        --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
        --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
        --v=2
      Restart=on-failure
      RestartSec=5

      [Install]
      WantedBy=multi-user.target
      EOF
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Move k8s controller-manager kubeconfig to /var/lib/kubernetes/
  shell:
    cmd: |
      cp {{ remote_user_home }}/kube-controller-manager.kubeconfig /var/lib/kubernetes/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Add kube-controller-manager service to systemd
  shell:
    cmd: |
      cat <<EOF | tee /etc/systemd/system/kube-controller-manager.service
      [Unit]
      Description=Kubernetes Controller Manager
      Documentation=https://github.com/kubernetes/kubernetes

      [Service]
      ExecStart=/usr/local/bin/kube-controller-manager \\
        --bind-address=0.0.0.0 \\
        --cluster-cidr=10.200.0.0/16 \\
        --cluster-name=kubernetes \\
        --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
        --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
        --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
        --leader-elect=true \\
        --root-ca-file=/var/lib/kubernetes/ca.pem \\
        --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
        --service-cluster-ip-range=10.32.0.0/24 \\
        --use-service-account-credentials=true \\
        --v=2
      Restart=on-failure
      RestartSec=5

      [Install]
      WantedBy=multi-user.target
      EOF
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Move k8skube-scheduler kubeconfig to /var/lib/kubernetes/
  shell:
    cmd: |
      cp {{ remote_user_home }}/kube-scheduler.kubeconfig /var/lib/kubernetes/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Add yaml configuration for kubescheduler
  shell:
    cmd: |
      cat <<EOF | tee /etc/kubernetes/config/kube-scheduler.yaml
      apiVersion: kubescheduler.config.k8s.io/v1alpha1
      kind: KubeSchedulerConfiguration
      clientConnection:
        kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
      leaderElection:
        leaderElect: true
      EOF
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Add kube-scheduler service to systemd
  shell:
    cmd: |
      cat <<EOF | tee /etc/systemd/system/kube-scheduler.service
      [Unit]
      Description=Kubernetes Scheduler
      Documentation=https://github.com/kubernetes/kubernetes

      [Service]
      ExecStart=/usr/local/bin/kube-scheduler \\
        --config=/etc/kubernetes/config/kube-scheduler.yaml \\
        --v=2
      Restart=on-failure
      RestartSec=5

      [Install]
      WantedBy=multi-user.target
      EOF
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: start & enable k8s services
  shell:
    cmd: |
      systemctl daemon-reload
      systemctl enable kube-apiserver kube-controller-manager kube-scheduler
      systemctl start kube-apiserver kube-controller-manager kube-scheduler
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Verification of K8s master components cluster
  shell:
    cmd: |
      kubectl get componentstatuses --kubeconfig {{ remote_user_home }}/admin.kubeconfig
      curl --cacert {{ remote_user_home }}/ca.pem -H "Host: kubernetes.default.svc.cluster.local" -i https://127.0.0.1:6443/healthz
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Create Cluster role  for accessing kubelet service 
  shell:
    cmd: |
      cat <<EOF | kubectl apply --kubeconfig {{ remote_user_home }}/admin.kubeconfig -f -
      apiVersion: rbac.authorization.k8s.io/v1beta1
      kind: ClusterRole
      metadata:
        annotations:
          rbac.authorization.kubernetes.io/autoupdate: "true"
        labels:
          kubernetes.io/bootstrapping: rbac-defaults
        name: system:kube-apiserver-to-kubelet
      rules:
        - apiGroups:
            - ""
          resources:
            - nodes/proxy
            - nodes/stats
            - nodes/log
            - nodes/spec
            - nodes/metrics
          verbs:
            - "*"
      EOF
  when: "'k8s-master-1' in inventory_hostname"
  register: cmd_out
- debug: msg="{{cmd_out.stdout}}"
  when: cmd_out.changed==True
- debug: msg="{{cmd_out.stderr}}"
  when: cmd_out.changed==True

- name: Bind Cluster Role to admin user
  shell:
    cmd: |
      cat <<EOF | kubectl apply --kubeconfig {{ remote_user_home }}/admin.kubeconfig -f -
      apiVersion: rbac.authorization.k8s.io/v1beta1
      kind: ClusterRoleBinding
      metadata:
        name: system:kube-apiserver
        namespace: ""
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: system:kube-apiserver-to-kubelet
      subjects:
        - apiGroup: rbac.authorization.k8s.io
          kind: User
          name: kubernetes
      EOF
  when: "'k8s-master-1' in inventory_hostname"
  register: cmd_out
- debug: msg="{{cmd_out.stdout}}"
  when: cmd_out.changed==True
- debug: msg="{{cmd_out.stderr}}"
  when: cmd_out.changed==True

- name: Clean Working Directory 
  file:
    state: absent
    path: "{{ remote_workspace }}"