---

- name: Clean Working Directory 
  file:
    state: absent
    path: "{{ remote_workspace }}"

- name: Creates Deployment Working Directory
  file:
    path: "{{ remote_workspace }}"
    state: directory

- name: Download and Extract etcd package
  shell:
    cmd: |
      cd {{ remote_workspace }} && wget -q --show-progress --https-only --timestamping \
      "https://github.com/etcd-io/etcd/releases/download/v3.4.10/etcd-v3.4.10-linux-amd64.tar.gz"
      cd {{ remote_workspace }} && tar -xvf etcd-v3.4.10-linux-amd64.tar.gz
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Move etcd binary to /usr/local/bin
  shell:
    cmd: |
      mv {{ remote_workspace }}/etcd-v3.4.10-linux-amd64/etcd* /usr/local/bin/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Update etcd config 
  shell:
    cmd: |
      mkdir -p /etc/etcd /var/lib/etcd
      chmod 700 /var/lib/etcd
      cp {{ remote_user_home }}/ca.pem {{ remote_user_home }}/kubernetes-key.pem {{ remote_user_home }}/kubernetes.pem /etc/etcd/
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Add etcd service to systemd
  shell:
    cmd: |
      INTERNAL_IP={{ ansible_default_ipv4.address }}
      ETCD_NAME=$(hostname -s)
      cat <<EOF | tee /etc/systemd/system/etcd.service
      [Unit]
      Description=etcd
      Documentation=https://github.com/coreos

      [Service]
      Type=notify
      ExecStart=/usr/local/bin/etcd \\
        --name ${ETCD_NAME} \\
        --cert-file=/etc/etcd/kubernetes.pem \\
        --key-file=/etc/etcd/kubernetes-key.pem \\
        --peer-cert-file=/etc/etcd/kubernetes.pem \\
        --peer-key-file=/etc/etcd/kubernetes-key.pem \\
        --trusted-ca-file=/etc/etcd/ca.pem \\
        --peer-trusted-ca-file=/etc/etcd/ca.pem \\
        --peer-client-cert-auth \\
        --client-cert-auth \\
        --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
        --listen-peer-urls https://${INTERNAL_IP}:2380 \\
        --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
        --advertise-client-urls https://${INTERNAL_IP}:2379 \\
        --initial-cluster-token etcd-cluster-0 \\
        --initial-cluster {{ hostvars['k8s-master-1']['inventory_hostname'] }}=https://{{ hostvars['k8s-master-1']['ansible_ssh_host'] }}:2380,{{ hostvars['k8s-master-2']['inventory_hostname'] }}=https://{{ hostvars['k8s-master-2']['ansible_ssh_host'] }}:2380,{{ hostvars['k8s-master-3']['inventory_hostname'] }}=https://{{ hostvars['k8s-master-3']['ansible_ssh_host'] }}:2380 \\
        --initial-cluster-state new \\
        --data-dir=/var/lib/etcd
      Restart=on-failure
      RestartSec=5

      [Install]
      WantedBy=multi-user.target
      EOF
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: start & enable etcd service
  shell:
    cmd: |
      systemctl daemon-reload
      systemctl enable etcd
      systemctl start etcd
  become: true
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Validate cluster
  shell:
    cmd: |
      ETCDCTL_API=3 etcdctl member list \
      --endpoints=https://127.0.0.1:2379 \
      --cacert=/etc/etcd/ca.pem \
      --cert=/etc/etcd/kubernetes.pem \
      --key=/etc/etcd/kubernetes-key.pem
  register: cmd_out
  become: true
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Clean Working Directory 
  file:
    state: absent
    path: "{{ remote_workspace }}"