---

- name: Clean Kubeconfig Directory in Workspace 
  file:
    state: absent
    path: "{{ remote_workspace }}/generated_kubeconfigs"

- name: Create Kubeconfig Directory in Workspace 
  file:
    path: "{{ remote_workspace }}/generated_kubeconfigs"
    state: directory

- name: Generate kubeconfig for Worker Nodes
  shell:
    cmd: |
      kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/ca.pem \
        --embed-certs=true \
        --server=https://{{ hostvars['k8s-master-1']['ansible_ssh_host'] }}:6443 \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/{{ item.value.name }}.kubeconfig

        kubectl config set-credentials system:node:{{ item.value.name }} \
          --client-certificate={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/{{ item.value.name }}.pem \
          --client-key={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/{{ item.value.name }}-key.pem \
          --embed-certs=true \
          --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/{{ item.value.name }}.kubeconfig

        kubectl config set-context default \
          --cluster=kubernetes-the-hard-way \
          --user=system:node:{{ item.value.name }} \
          --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/{{ item.value.name }}.kubeconfig

        kubectl config use-context default --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/{{ item.value.name }}.kubeconfig
  with_dict: "{{ vms }}"
  when: item.value.name is search(".*worker.*")
  register: cmd_out
- debug: msg="item.item.key={{item.item.key}}, item.stdout={{item.stdout}}"
  when: item.changed==True
  with_items: "{{ cmd_out.results }}"
- debug: msg="item.item.key={{item.item.key}}, item.stdout={{item.stderr}}"
  when: item.changed==True
  with_items: "{{ cmd_out.results }}"

- name: Generate kubeconfig for kube-proxy
  shell:
    cmd: |
      kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/ca.pem \
        --embed-certs=true \
        --server=https://{{ hostvars['k8s-master-1']['ansible_ssh_host'] }}:6443 \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-proxy.kubeconfig

      kubectl config set-credentials system:kube-proxy \
        --client-certificate={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-proxy.pem \
        --client-key={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-proxy-key.pem \
        --embed-certs=true \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-proxy.kubeconfig

      kubectl config set-context default \
        --cluster=kubernetes-the-hard-way \
        --user=system:kube-proxy \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-proxy.kubeconfig

      kubectl config use-context default --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-proxy.kubeconfig
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Generate kubeconfig for kube-controller-manager
  shell:
    cmd: |
      kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/ca.pem \
        --embed-certs=true \
        --server=https://127.0.0.1:6443 \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-controller-manager.kubeconfig

      kubectl config set-credentials system:kube-controller-manager \
        --client-certificate={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-controller-manager.pem \
        --client-key={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-controller-manager-key.pem \
        --embed-certs=true \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-controller-manager.kubeconfig

      kubectl config set-context default \
        --cluster=kubernetes-the-hard-way \
        --user=system:kube-controller-manager \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-controller-manager.kubeconfig

      kubectl config use-context default --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-controller-manager.kubeconfig
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Generate kubeconfig for kube-scheduler
  shell:
    cmd: |
      kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/ca.pem \
        --embed-certs=true \
        --server=https://127.0.0.1:6443 \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-scheduler.kubeconfig

      kubectl config set-credentials system:kube-scheduler \
        --client-certificate={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-scheduler.pem \
        --client-key={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/kube-scheduler-key.pem \
        --embed-certs=true \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-scheduler.kubeconfig

      kubectl config set-context default \
        --cluster=kubernetes-the-hard-way \
        --user=system:kube-scheduler \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-scheduler.kubeconfig

      kubectl config use-context default --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/kube-scheduler.kubeconfig
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Generate kubeconfig for Admin
  shell:
    cmd: |
      kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/ca.pem \
        --embed-certs=true \
        --server=https://127.0.0.1:6443 \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/admin.kubeconfig

      kubectl config set-credentials admin \
        --client-certificate={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/admin.pem \
        --client-key={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_certs/admin-key.pem \
        --embed-certs=true \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/admin.kubeconfig

      kubectl config set-context default \
        --cluster=kubernetes-the-hard-way \
        --user=admin \
        --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/admin.kubeconfig

      kubectl config use-context default --kubeconfig={{ remote_user_home }}/{{ repo_directory }}/artifacts/generated_kubeconfigs/admin.kubeconfig
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Copy Generated Kubeconfigs to Artifacts Directory
  shell:
    cmd: |
      cd {{ remote_user_home }}/{{ repo_directory }}/artifacts && cp -R {{ remote_workspace }}/generated_kubeconfigs .
  register: cmd_out
- debug: msg="{{ cmd_out.stdout }}"
- debug: msg="{{ cmd_out.stderr }}"

- name: Clean Kubeconfig Directory in Workspace 
  file:
    state: absent
    path: "{{ remote_workspace }}/generated_kubeconfigs"